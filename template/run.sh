#!/bin/bash

if [ -f /mnt/setup.sh ]; then
  bash /mnt/setup.sh
else
  echo 'Setup script not found'
  /bin/bash
fi
