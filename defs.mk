# Comment the CY_PROXY_ENABLE to not use apt-cacher-ng when building and running images/containers
CY_PROXY_ENABLE := TRUE

CY_PROXY := cyproxy
CY_PROXY_PORT := 3128

# To get container id, pass the name of the container as argument
GET_CONT_ID_BY_NAME = $(shell sudo docker ps -a -q --filter name=$(1) --format="{{.ID}}")
GET_CONT_ID_BY_ANCESTOR = $(shell sudo docker ps -a -q --filter ancestor=$(1) --format="{{.ID}}")

# To get image id, pass the name of the image as argument
GET_IMG_ID = $(shell sudo docker images -q $(1))

# To get IP of a container, pass the container ID as argument
GET_CONT_IP = $(shell sudo docker inspect --format '{{ .NetworkSettings.IPAddress }}' $(1))
