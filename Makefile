include defs.mk

.PHONY: $(CY_PROXY) all clean template

SUBDIRS = cyproxy template

all:
	for dir in $(SUBDIRS); do \
		$(MAKE) -C $$dir; \
	done

clean:
	#sudo docker rmi $(shell sudo docker images -q ${ROOT_DIR})
	for dir in ${SUBDIRS}; do \
		$(MAKE) -C $$dir clean; \
	done
