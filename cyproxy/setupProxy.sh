#!/bin/bash

#set -x
set -e

############################ Proxy server#####################################
CONT_LIST=$(sudo docker ps -a | grep proxy | cut -d' ' -f1)
if [ "$CONT_LIST" != "" ]; then
  # docker rm -f "$CONT_LIST"
  echo 'Proxy already running, nothing to do'
else
  echo 'Building cyproxy ...'
  proxyDIR=$(pwd)/../cyproxy
  sudo docker run --name cyproxy -h=cyproxy -d --restart=always \
    --publish 3128:3128 \
    --volume ${proxyDIR}/squid.conf:/etc/squid3/squid.conf \
    --volume ${proxyDIR}/cache:/var/spool/squid3 \
    sameersbn/squid:3.3.8-23
fi

